/*2. Взять SLIDER с урока, и добавить новый функционал, а именно:
- при нажатии на точки снизу слайдера мы должны попадать на нужный нам слайд.
Нажимаем на первую точку - мы на первом слайде
Нажимаем на вторую точку - мы на втором слайде, и тд
- активная точка должна закрашиваться цветом ( цвет на ваш выбор )
- доработать слайдер так, чтобы после последнего слайда шел первый, и у нас начинался слайдер заново
- когда мы дошли до первого слайда , сделать так , чтобы слайдер не останавливался а переходил на последний слайд
вообщем слайдер должен работать циклично*/

const prev = document.getElementById('btn-prev'),
    next = document.getElementById('btn-next'),
    slides = document.querySelectorAll('.slide'),
    dots = document.querySelectorAll('.dot');

let index = 0;

function activeSlide(n) {
    for (slide of slides) {
        slide.classList.remove('active');
    }
    slides[n].classList.add('active');
}
function activeDot(ind) {
    for (dot of dots) {
        dot.classList.remove('active');
    }
    dots[ind].classList.add('active');
}
function prepareCurrentSlide(ind) {
    activeSlide(ind);
    activeDot(ind);
}
function nextSlide() {
    if (index == slides.length - 1) {
        index = 0;
        prepareCurrentSlide(index);
    } else {
        index++;
        prepareCurrentSlide(index);
    }
}
function prevSlide() {
    if (index == 0) {
        index = slides.length - 1;
        prepareCurrentSlide(index);
    } else {
        index--;
        prepareCurrentSlide(index);
    }
}
dots.forEach(function (item, indexDot) {
    item.addEventListener('click', function () {
        index = indexDot;
        prepareCurrentSlide(index);
    })
});
next.addEventListener('click', nextSlide);
prev.addEventListener('click', prevSlide);

const interval = setInterval(nextSlide, 2500);